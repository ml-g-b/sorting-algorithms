(*
--------------------------------------------------
| Tri fusion utilisé pour la création de tableau | 
--------------------------------------------------
*)

let rec renverse_bis l lf=
match l with
  []->lf
|x::r->renverse_bis r (x::lf);;

let renverse l=
renverse_bis l [];;

let rec divise_en_deux_bis l i l1=
if i>=0
then divise_en_deux_bis (List.tl l) (i-1) ((List.hd l)::l1)
else (renverse l1,l);; (*Cran de sortie, en effet on est à la moitié du parcours donc on renvoie le couple l1 contenant la 1e moitié de la liste et l le reste*)

let divise_en_deux l=
divise_en_deux_bis l (((List.length l)-1)/2) [];; (*Initilaisation de l1 et i *) 
(*(((List.length l)-1)/2) cela correspond à l'indice du milieu et si il y a 2k éléments alors le milieu sera (k-1)*)

let rec fusion comp l1 l2=
match (l1, l2) with (*On va tester toutes les possibilités*)
  ([],[])->[]  
|([],y::r2)->l2(*On gère le cas où les listes sont de longueurs différentes*)
|(x::r1,[])->l1
|(x::r1,y::r2)->if comp x y
    then x::(fusion comp r1 l2)
    else y::(fusion comp l1 r2);;

let rec tri_fusion_bis comp l=
match l with
  []->[]
|[x]->l
|x::r->let (l1,l2)=divise_en_deux l in 
    fusion comp (tri_fusion_bis comp l1) (tri_fusion_bis comp l2);; (*On décompose en sous listes triées naturellement*)

let tri_fusion comp l=
if l=[]
then failwith "Liste vide"
else tri_fusion_bis comp l;;

(*
-----------------------
| Création de tableau | 
-----------------------
*)

let rec liste_aleatoire_grand nb_entiers borne_max lf=
if nb_entiers<=0 
then reverse lf
else liste_aleatoire_grand (nb_entiers-1) borne_max ((Random.int borne_max)::lf);;
  
let rec liste_aleatoire nb_entiers borne_max=
 if nb_entiers<=0||borne_max<=0 
then []
else if nb_entiers >= 3000
  then liste_aleatoire_grand nb_entiers borne_max []
  else (Random.int borne_max)::(liste_aleatoire (nb_entiers-1) borne_max);;

let tableau nb_entiers borne_max comp=
  let l = liste_aleatoire nb_entiers borne_max in let lt = tri_fusion comp l in Array.of_list lt;;


(*
--------------------------
| Recherche sequentielle | 
--------------------------
*)

let rec recherche_sequentielle_aux e t i=
  if i<0
  then false
  else if e=t.(i)
  then true
  else recherche_sequentielle_aux e t (i-1);;

let recherche_sequentielle e t=
  recherche_sequentielle_aux e t ((Array.length t )-1);;

(*
--------------------------
| Recherche dichotomique | 
--------------------------
*)

let rec recherche_dichotomique_aux comp e t imin imax=
  if imin+1>=imax
  then e=t.(imin)||e=t.(imax)
  else let milieu = (imax+imin)/2 in
    if comp e t.(milieu)
    then recherche_dichotomique_aux comp e t imin milieu
    else recherche_dichotomique_aux comp e t milieu imax;;

let recherche_dichotomique comp e t=
  recherche_dichotomique_aux comp e t 0 ((Array.length t)-1);;

(* (comp e t.(imin))||(comp e t.(imax)) *)
(*
----------------------------------------
| Recherche dichotomique exponentielle | 
----------------------------------------
*)
let rec indices_initiaux comp e t i=
  if i>=Array.length t|| comp e t.(i)
  then if i+1>=Array.length t
    then (i/2,(Array.length t)-1)
    else (i/2,i+1)
  else indices_initiaux comp e t (i*2);;

let recherche_dichotomique_exp comp e t=
  let (imin,imax)=indices_initiaux comp e t 1 in 
  recherche_dichotomique_aux comp e t imin imax;;

(*
---------
| Tests | 
---------
*)

let max = ((max_int-1)/2);;
let t = tableau 100000 max (<);;

let rec trouver f t l= (*Cette fonction permet de tester chacun des éléments du tableau*)
match l with
[]->[]
|x::r->(f x t)::(trouver f t r);; (* La seule façon trouvée pour tester chaque élément est de faire une sorte de fontion map : on crée une bool list [true;false;false...] avec les résultats de la focntion f*)

(* Cette méthode ne fourni pas de résultats concluants

let temps_debut = Sys.time () in let _ = recherche_sequentielle e t in let temps_fin = Sys.time () in (temps_fin -. temps_debut);;
let temps_debut = Sys.time () in let _ = recherche_dichotomique (>) e t in let temps_fin = Sys.time () in (temps_fin -. temps_debut);;
let temps_debut = Sys.time () in let _ = recherche_dichotomique_exp (>) e t in let temps_fin = Sys.time () in (temps_fin -. temps_debut);;*)


let temps_debut = Sys.time () in let _ = trouver (recherche_sequentielle) t l in let temps_fin = Sys.time () in (temps_fin -. temps_debut);;

let temps_debut = Sys.time () in let _ = trouver (recherche_dichotomique (<)) t l in let temps_fin = Sys.time () in (temps_fin -. temps_debut);;

let temps_debut = Sys.time () in let _ = trouver (recherche_dichotomique_exp (<)) t l in let temps_fin = Sys.time () in (temps_fin -. temps_debut);;

(* Ici la fonction f de trouver est (recherche dichotomique (<)) en effet on appelle la fonction avec 1 argument *)