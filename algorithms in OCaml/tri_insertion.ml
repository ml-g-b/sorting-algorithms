let rec insere comp e l=  
  match l with    
    [] - [e]           
   xr - if comp e x    
      then el                
      else x(insere comp e r);;

let rec tri_insertion_bis comp l1 l2=   
  match l1 with    
    [] - l2  
   xr - tri_insertion_bis comp r (insere comp x l2);;

let tri_insertion comp l=
  tri_insertion_bis comp l [];;