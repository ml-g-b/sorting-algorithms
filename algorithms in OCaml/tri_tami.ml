let rec nieme_bis l i=
  match l with
    []->failwith "nieme_bis : Liste vide"
  |x::r->if i = 0
      then x
      else nieme_bis r (i-1);;

let nieme l i=
  if i>=List.length l
  then failwith "nieme : Problème d'indice"
  else nieme_bis l i;;

let rec permuter_bis l i j a n1 n2=
  match l with
    []->[]
  |x::r->if a = i
      then n2::(permuter_bis r i j (a+1) n1 n2)
      else if a=j
      then n1::(permuter_bis r i j (a+1) n1 n2)
      else x::(permuter_bis r i j (a+1) n1 n2);;

let rec permuter l i j=
  let n1 = nieme l i and n2 = nieme l j in
  permuter_bis l i j 0 n1 n2;;

let rec descente_bulle comp l i=
  if i>0 && comp (nieme l i) (nieme l (i-1))  
  then descente_bulle comp (permuter l i (i-1)) (i-1)
  else l;;

let rec tri_tami_bis comp l i=
  if i+1>=List.length l
  then l
  else if comp (nieme l (i+1)) (nieme l i) 
  then tri_tami_bis comp (descente_bulle comp (permuter l i (i+1)) (i)) (i+1)
  else tri_tami_bis comp l (i+1);;

let tri_tami comp l=
  tri_tami_bis comp l 0;;

tri_tami (<) [2;8;4;6;3;7;1;9;5;0];;