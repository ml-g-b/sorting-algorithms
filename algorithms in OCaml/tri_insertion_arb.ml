type 'a arbreBinaire =     
    ArbreVide  
  | Noeud of ('a * 'a arbreBinaire * 'a arbreBinaire);;

let rec infixe a =  
  match a with    
    ArbreVide -> []  
  | Noeud (r, g, d) -> (infixe g) @ [r] @ (infixe d) ;;

let rec insere_abr comp e arbre =  
  match arbre with    
    ArbreVide -> Noeud(e, ArbreVide, ArbreVide)  
  | Noeud (racine, gauche, droite) ->if comp e racine  
      then Noeud(racine, (insere_abr comp e gauche), droite)      
      else Noeud(racine, gauche, (insere_abr comp e droite));; 

let rec tri_insert_bis comp l arbre=
  match l with
    []->infixe arbre
  |x::r->tri_insert_bis comp r (insere_abr comp x arbre);;

let tri_insert comp l=
  match l with
    []->[]
  |x::r->tri_insert_bis comp r (Noeud (x,ArbreVide,ArbreVide));;

