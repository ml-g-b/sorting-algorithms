let rec partition_bis comp l pivot l1 l2 l3=
  match l with
    []->(l1,l2,l3) (* Au final on a 3 listes : dans l1, les éléments pour lesquels la condition est statisfaite, dans l2, ceux pour qui ce n'est pas satisfait et dans l3 ceux qui sont égaux à l3 *)
  |x::r->if x=pivot (*On compare la tete de liste (le 1e élément) avec le pivot*)
      then partition_bis comp r pivot l1 l2 (x::l3) (* Si c'est égal, on met x dans l3 (x::[] donne [x]; 1::[2;3;4] donne [1;2;3;4])*)
      else if comp x pivot (* Pas égal donc on compare x et pivot *)
      then partition_bis comp r pivot (x::l1) l2 l3 (* Condition satisfaite donc x::l1 *)
      else partition_bis comp r pivot l1 (x::l2) l3 (* Sinon x::l2*)
          
let partition comp l pivot=
  partition_bis comp l pivot [] [] [];; (* On appelle la fonction auxiliaire avec l1,l2,l3 initialisée en liste vide []*)

let rec tri_rapide comp l=
  match l with
    []->[]
  |x::r->let (l1,l2,l3)=partition comp r x in (* On découpe la liste selon comp avec x comme pivot*)
      (tri_rapide comp l1)@l3@(x::(tri_rapide comp l2));; (* @ est une fonction qui permet de concaténer deux listes exemple : [1;2;3;4]@[5;6;7;8] donne [1;2;3;4;5;6;7;8], on ne peut concaténer que deux listes de mêmes types*)
(* ici comp est une fonction polymorphe de comparaison,
exemple 3 < 5 est true et est la même chose que (<) 3 5, on met la fonction de comparaison en préfixe*)

(* Cela fonctionne aussi avec "3 + 2" = "(+) 3 2" = "5" *)

(* x::r signifie un élément x suivie d'un reste exemple: dans [1;2;3;4;5], on peut dire 1::[2;3;4;5] et pour [9] on peut dire 9::[]*)